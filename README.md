# Домашнее задание к занятию "3.3. Операционные системы, лекция 1" #

> 1. Какой системный вызов делает команда cd? В прошлом ДЗ мы выяснили, что cd не является самостоятельной программой, это shell builtin, поэтому запустить strace непосредственно на cd не получится. Тем не менее, вы можете запустить strace на /bin/bash -c 'cd /tmp'. В этом случае вы увидите полный список системных вызовов, которые делает сам bash при старте. Вам нужно найти тот единственный, который относится именно к cd. Обратите внимание, что strace выдаёт результат своей работы в поток stderr, а не в stdout.

Сначала запускается системный вызов newfstatat (общий интерфейс для доступа к информации о файле), следующий системный вызов chdir изменяет рабочую директорию.

> 2. Попробуйте использовать команду file на объекты разных типов на файловой системе. Например:

```
    vagrant@netology1:~$ file /dev/tty
    /dev/tty: character special (5/0)
    vagrant@netology1:~$ file /dev/sda
    /dev/sda: block special (8/0)
    vagrant@netology1:~$ file /bin/bash
    /bin/bash: ELF 64-bit LSB shared object, x86-64
```

> Используя strace выясните, где находится база данных file на основании которой она делает свои догадки.

База в которой осуществляет поиск сигнатуры команда file находится **/usr/share/misc/magic.mgc**

Вот системный вызов:

```
	openat(AT_FDCWD, "/usr/share/misc/magic.mgc", O_RDONLY) = 3
```

> 3. Предположим, приложение пишет лог в текстовый файл. Этот файл оказался удален (deleted в lsof), однако возможности сигналом сказать приложению переоткрыть файлы или просто перезапустить приложение – нет. Так как приложение продолжает писать в удаленный файл, место на диске постепенно заканчивается. Основываясь на знаниях о перенаправлении потоков предложите способ обнуления открытого удаленного файла (чтобы освободить место на файловой системе).

Находим нужный PID процесса который пишет в файл **lsof | grep <имя файла>** и открытый файловый дескриптор
Зануляем файл **cat /dev/null > /proc/<PID процесса>/fd/<Файловый дескриптор>** 


> 4. Занимают ли зомби-процессы какие-то ресурсы в ОС (CPU, RAM, IO)?

Зомби-процессы не занимают ресурсов, только блокируют записи в таблице процессов. Размер таблицы процессов ограничен для каждого пользователя и системы в целом. Ввиду этого придостижении лимита записей в таблице процессов, "нормальные" процессы не могут создаваться, даже пользователь не сможет войти в систему.

> 5. В iovisor BCC есть утилита opensnoop:

```
    root@vagrant:~# dpkg -L bpfcc-tools | grep sbin/opensnoop
    /usr/sbin/opensnoop-bpfcc
```

> На какие файлы вы увидели вызовы группы open за первую секунду работы утилиты? Воспользуйтесь пакетом bpfcc-tools для Ubuntu 20.04.

```
	PID    COMM               FD ERR PATH
	1666   irqbalance          7   0 /proc/interrupts
	1666   irqbalance          7   0 /proc/stat
	1666   irqbalance          7   0 /proc/irq/1/smp_affinity
	1666   irqbalance          7   0 /proc/irq/3/smp_affinity
	1666   irqbalance          7   0 /proc/irq/4/smp_affinity
	1666   irqbalance          7   0 /proc/irq/11/smp_affinity
	1666   irqbalance          7   0 /proc/irq/15/smp_affinity
	1666   irqbalance          7   0 /proc/irq/16/smp_affinity
	1666   irqbalance          7   0 /proc/irq/17/smp_affinity
	1666   irqbalance          7   0 /proc/irq/20/smp_affinity
	1666   irqbalance          7   0 /proc/irq/22/smp_affinity
	1666   irqbalance          7   0 /proc/irq/23/smp_affinity
	1666   irqbalance          7   0 /proc/irq/27/smp_affinity
	1666   irqbalance          7   0 /proc/irq/28/smp_affinity
	1666   irqbalance          7   0 /proc/irq/34/smp_affinity
	1666   irqbalance          7   0 /proc/irq/35/smp_affinity
```

> 6. Какой системный вызов использует uname -a? Приведите цитату из man по этому системному вызову, где описывается альтернативное местоположение в /proc, где можно узнать версию ядра и релиз ОС.

Используется системный вызов uname().
Цитата из man:
```
	Part of the utsname information is also accessible via /proc/sys/kernel/{ostype, hostname, osrelease, version, domainname}.
```

> 7. Чем отличается последовательность команд через ; и через && в bash? Например:

```
    root@netology1:~# test -d /tmp/some_dir; echo Hi
    Hi
    root@netology1:~# test -d /tmp/some_dir && echo Hi
    root@netology1:~#
```

; - разделитель последовательных команд, а && - условный олператор (используется для объединения команд таким образом, что следующая команда запускается тогда и только тогда, когда предыдущая команда завершилась без ошибок или, точнее, выйдет с кодом возврата 0).

> Есть ли смысл использовать в bash &&, если применить set -e?

**set -e** останавливает выполнение скрипта если в комманде произошла ошибкачто противоположно поведению оболочки по умолчанию. Думаю, в таком случае это равнозначные команды.

> 8. Из каких опций состоит режим bash set -euxo pipefail и почему его хорошо было бы использовать в сценариях?

**-e** - прерывает выполнение исполнения при ошибке любой команды кроме последней

**-x** - вывод трейса простых команд 

**-u** - неустановленные / не заданные параметры и переменные считаются как ошибки, с выводом в stderr текста ошибки и выполнит завершение неинтерактивного вызова

**-o** - pipefail возвращает код возврата последовательности команд

Повышает детализацию ошибок выполнения, а так же завершит сценарий при наличии ошибок на любом из этапов выполнения сценария.

> 9. Используя -o stat для ps, определите, какой наиболее часто встречающийся статус у процессов в системе. В man ps ознакомьтесь (/PROCESS STATE CODES) что значат дополнительные к основной заглавной буквы статуса процессов. Его можно не учитывать при расчете (считать S, Ss или Ssl равнозначными).

Наиболее часто встречающийся статус у процессов  S, Ss или Ssl.
Вывод **ps -ao pid,stat**
```
  PID STAT
  38240 S
  38241 S
  38242 S
  38655 S+
  38695 T
  38707 T
  39997 R+
```
```
PROCESS STATE CODES
       Here are the different values that the s, stat and state output specifiers (header "STAT" or "S") will display to describe the state
       of a process:

               D    uninterruptible sleep (usually IO)
               I    Idle kernel thread
               R    running or runnable (on run queue)
               S    interruptible sleep (waiting for an event to complete)
               T    stopped by job control signal
               t    stopped by debugger during the tracing
               W    paging (not valid since the 2.6.xx kernel)
               X    dead (should never be seen)
               Z    defunct ("zombie") process, terminated but not reaped by its parent
```